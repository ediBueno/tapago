@php
  $strong = '';
@endphp

<div class="alert alert-{{ Session::get('flash_alert_type') }} alert-dismissible {{ (Session::has('flash_alert'))? 'show' : 'hide' }}">
  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
  <h4>
    @if (Session::get('flash_alert_type') == 'success' )
       <i class="icon fa fa-check"></i> Sucesso!
    @endif
    @if (Session::get('flash_alert_type') == 'warning' )
       <i class="icon fa fa-ban"></i> Ops!
    @endif
    @if (Session::get('flash_alert_type') == 'danger' )
       <i class="icon fa fa-close"></i> Erro!
    @endif
    </h4>
  {{ Session::get('flash_alert') }}
</div>
