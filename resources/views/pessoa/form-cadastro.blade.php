<form class="" action="{{ isset($pessoa) ? Route('pessoa.edit' , $pessoa->id) : Route('pessoa.create') }}" id="formSubmitPessoa" method="post">
  @csrf
  <div class="row">
        <div class="form-group col-xs-6">
            <label>Nome:</label>
            <input type="text" class="form-control" id="nome" name="nome" value="" placeholder="Nome">
        </div>
      <div class="form-group col-xs-6">
          <label>Cpf:</label>
          <input type="text" class="form-control" id="cpf" name="cpf" value="" placeholder="Cpf">
      </div>
  </div>
  <div class="row">
        <div class="form-group col-xs-4">
            <label>Data Nascimento:</label>
            <input type="date" class="form-control" id="data_nascimento" name="data_nascimento"  value="" placeholder="Data Nascimento">
        </div>

        <div class="form-group col-xs-4">
            <label>Telefone:</label>
            <input type="text" class="form-control" id="telefone" name="telefone"  value="" placeholder="Telefone">
        </div>
        <div class="form-group col-xs-4">
            <label>Email:</label>
            <input type="email" class="form-control" id="email" name="email" value="" placeholder="Email">
        </div>
  </div>
    <div >
            <div class="col-lg-4 col-xs-6">
              <button type="button"  onclick="location.href='{{ Route('pessoa.index') }}'" class="btn bg-orange btn-flat form-control ">Limpar</button>
            </div>

            <div class="col-lg-4 col-xs-6">
                <button type="submit" id="btnSubmitPessoa"  class="btn bg-green btn-flat  form-control">Salvar</button>
            </div>
    </div>

</form>
