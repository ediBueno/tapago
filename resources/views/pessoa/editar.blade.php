@extends('layouts.sistema')

@section('container')


    <div class="row ">
        <div class="col-md-12">
          <div class="nav-tabs-custom">
              <ul class="nav nav-tabs">
                  <li class="active"><a href="#tab_1" data-toggle="tab">Dados pessoais</a></li>
                  <li class=""><a href="#tab_2" data-toggle="tab">Endereços</a></li>

              </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="tab_1">
                  <div class="row">
                    <div class="col-md-8">


                          @include('pessoa.form-cadastro')
                        </div>
                  </div>
              </div>

              <div class="tab-pane" id="tab_2">
                <div class="row">
                  <div class="col-md-8">


                        @include('endereco.form-endereco')
                      </div>
                </div>
              <div class="margin">

              </div>
                <div class="row">
                  <div class="col-md-12">


                          <table id="table-listagem-enderecos">
                              <thead>
                                  <th>Estado</th>
                                  <th>Cidade</th>
                                  <th>Bairro</th>
                                  <th>Rua</th>
                                  <th>Número</th>
                                  <th>Complemento</th>
                                  <th>Ações</th>
                              </thead>
                              <tbody>

                              </tbody>
                          </table>
                      </div>
                </div>
            </div>
            </div>
          </div>
        </div>

  @endsection
  @section('scripts')
<script type="text/javascript">
  $(document).ready(function () {
    $.noConflict();

  $( "#formSubmitPessoa" ).submit(function( event ) {

        event.preventDefault();
        var data = $( this ).serialize();
        var url = $( this ).attr('action');
        $.ajax({
            url: url ,
             type: "PUT",
            dataType:'json',
            data: data,
            success: function(data){
                  alert(data.msg);
                 $('#table-listagem-enderecos').DataTable().draw();
            }
        });
  });

  $.ajax({
      url: "{{ Route('pessoa.edit'  , $pessoa->id ) }}" ,
      type: "GET",
      dataType:'json',
      data: { '_token' : "{{ csrf_token() }}" } ,
      success: function(data){

        $('#nome').val(data.nome);
        $('#cpf').val(data.cpf);
        $('#data_nascimento').val(data.data_nascimento);
        $('#telefone').val(data.telefone);
        $('#email').val(data.email);


            var $linhas = '';
           $.each(data.enderecos, function (key, item) {

               $linhas = '<tr id="row-end-'+item.id+'"><td>'+item.uf+'</td><td>'+item.cidade+'</td><td>'+item.bairro+'</td><td>'+item.endereco+'</td><td>'+item.numero_endereco+'</td><td>'+item.complemento+'</td> <td><button type="button" class="btn btn-warning btnEdtEnd" idend="'+item.id+'"  name="button">Editar</button>&nbsp;<button type="button" class="btn btn-danger btnDelEnd" idend="'+item.id+'"  name="button">Excluir</button></td></tr>';

                 $('#table-listagem-enderecos tbody').append($linhas);
            });

            var table = $('#table-listagem-enderecos').DataTable( {
                "language": {
                         "url": "https://cdn.datatables.net/plug-ins/1.10.12/i18n/Portuguese-Brasil.json"
                  },
            } );

      }
  });



  $( "#formCadastroEndereco" ).submit(function( event ) {
        event.preventDefault();
        var data = $( this ).serialize();
        //var url = $( this ).attr('action');
        var url = $('#id-end').val() == '' ? "{{ Route('endereco.create')}}/{{ $pessoa->id }}" :  "{{ Route('endereco.update')}}/"+$('#id-end').val() ;
        var type = $('#id-end').val() == '' ?  'POST' :  'PUT' ;
        $.ajax({
            url: url ,
             type: type,
            dataType:'json',
            data: data,
            success: function(data){
                var item = data.endereco ;

                $('#row-end-'+item.id).remove();

                $linhas = '<tr id="row-end-'+item.id+'"><td>'+item.uf+'</td><td>'+item.cidade+'</td><td>'+item.bairro+'</td><td>'+item.endereco+'</td><td>'+item.numero_endereco+'</td><td>'+item.complemento+'</td> <td><button type="button" class="btn btn-warning btnEdtEnd" idend="'+item.id+'"  name="button">Editar</button>&nbsp;<button type="button" class="btn btn-danger btnDelEnd" idend="'+item.id+'"  name="button">Excluir</button></td></tr>';
                $('#table-listagem-enderecos tbody').append($linhas);
                 $('#table-listagem').DataTable().draw();
            }
        });
  });

  $( 'body' ).on( 'click' , '.btnEdtEnd' , function( event ) {
        var id = $(this).attr('idend');
        $.ajax({
            url: "{{ Route('endereco.show') }}/"+id+"" ,
             type: 'GET',
            dataType:'json',
            success: function(data){

                  $('#id-end').val(data.id);
                  $('#estado option[value='+data.uf+']').attr('selected','selected');
                  $('#cidade').val(data.cidade);
                  $('#bairro').val(data.bairro);
                  $('#endereco').val(data.endereco);
                  $('#numero_endereco').val(data.numero_endereco);
                  $('#complemento').val(data.complemento);
            }
        });
  });


    $( 'body' ).on( 'click' , '.btnDelEnd' , function( event ) {

        if (confirm('Deseja realmente excluir o endereço?')) {

          var id = $(this).attr('idend');
          $.ajax({
              url: "{{ Route('endereco.delete') }}/"+id+"" ,
               type: 'DELETE',
              dataType:'json',
              success: function(data){

                $('#row-end-'+id).remove();
                 $('#table-listagem-enderecos').DataTable().draw();

              }
          });
          }
    });



  $('#btn-limpar').click(function() {
      $('#formCadastroEndereco').each(function () {
          this.reset();
       });
   });

  });

  </script>
  @endsection
