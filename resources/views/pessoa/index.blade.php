@extends('layouts.sistema')

@section('container')
  <div class="row">

        <div class="col-md-8">

              <div class="box box-default box-solid ">
                <div class="box-header with-border">
                  <h3 class="box-title">Cadastrar pessoa</h3>

                  <!-- /.box-tools -->
                </div>
                <!-- /.box-header -->
                <div class="box-body" >
                  @include('pessoa.form-cadastro')
                </div>

            </div>

        </div>
  </div>

  <div class="row">

        <div class="col-md-12">

          <div class="box box-success ">
            <div class="box-header with-border">
              <h3 class="box-title">Pessoas</h3>

            </div>
            <!-- /.box-header -->
            <div class="box-body" >
                <table id="table-listagem" style="width:100%" >
                  <thead>
                          <th></th>
                          <th>#</th>
                          <th>Nome</th>
                          <th>Cpf</th>
                          <th>Data nascimento</th>
                          <th>Telefone</th>
                          <th>Email</th>
                          <th>Ações</th>
                  </thead>
                  <tbody>

                  </tbody>
                </table>
            </div>

        </div>

      </div>
  </div>
@endsection

@section('scripts')
<script type="text/javascript">
$(document).ready(function () {
  $.noConflict();

  /* Formatting function for row details - modify as you need */
function format ( d ) {
    // `d` is the original data object for the row
    subtable = '<table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;">'+
                  '<tr>'+
                      '<td>Estado:</td>'+
                      '<td>Cidade:</td>'+
                      '<td>Bairro:</td>'+
                      '<td>Rua:</td>'+
                      '<td>Numero:</td>'+
                  '</tr>';

                  $.each(d.enderecos , function(i, item) {
                    subtable += '<tr>'+
                                    '<td>'+item.estado+'</td>'+
                                    '<td>'+item.cidade+'</td>'+
                                    '<td>'+item.bairro+'</td>'+
                                    '<td>'+item.rua+'</td>'+
                                    '<td>'+item.numero +'</td>'+
                                '</tr>';
                  });



    subtable += '</table>';

    return subtable ;
}

$(document).ready(function() {

    var table = $('#table-listagem').DataTable( {

        "language": {
                 "url": "https://cdn.datatables.net/plug-ins/1.10.12/i18n/Portuguese-Brasil.json"
          },
      "processing": true,
       "serverSide": true,
       "ajax": {
           "url": "{{ Route('pessoa.store') }}" ,

           "data" : { '_token' : "{{ csrf_token() }}" },


       },

        "columns": [
            {
                "className":      'details-control',
                "orderable":      false,
                "data":           null,
                "defaultContent": ''
            },
            { "data": "id" },
            { "data": "nome" },
            { "data": "cpf" },
            { "data": "data_nascimento" },
            { "data": "telefone" },
            { "data": "email" },
            { "data" : "btnEditar" }
        ],
        "order": [[1, 'asc']]
    } );

    // Add event listener for opening and closing details
    $('#table-listagem tbody').on('click', 'td.details-control', function () {
        var tr = $(this).closest('tr');
        var row = table.row( tr );

        if ( row.child.isShown() ) {
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
        }
        else {
            // Open this row
            row.child( format(row.data()) ).show();
            tr.addClass('shown');
        }
    } );
} );


$( "#formSubmitPessoa" ).submit(function( event ) {

      event.preventDefault();
      var data = $( this ).serialize();
      var url = $( this ).attr('action');
      if ($('#nome').val() == '') {
        $('#nome').parent('.form-group').addClass('has-error');
        $('#nome').parent('.form-group').append('<small class="text-danger" > O campo nome é obriatório! </small>');
      }else{
      $.ajax({
          url: url ,
           type: "POST",
          dataType:'json',
          data: data,
          success: function(data){

               $('#table-listagem').DataTable().draw();
          }
      });
    }
});


});
</script>
@endsection
