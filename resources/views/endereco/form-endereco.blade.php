<form class="" id="formCadastroEndereco" action="{{ Route('endereco.create') }}" method="post">
  @csrf
  <input type="hidden" name="" id="id-end" value="">
  <div class="row">
        <div class="form-group col-xs-6">
            <label>Rua:</label>
            <input type="text" class="form-control" id="endereco" name="endereco" value="" placeholder="Rua">
        </div>

        <div class="form-group col-xs-2">
            <label>Número:</label>
            <input type="text" class="form-control" id="numero_endereco" name="numero_endereco" value="" placeholder="Número">
        </div>


      <div class="form-group col-xs-4">
          <label>Bairro:</label>
          <input type="text" class="form-control" id="bairro" name="bairro" value="" placeholder="Bairro">
      </div>
  </div>
  <div class="row">
        <div class="form-group col-xs-4">
            <label>Estado:</label>
            <select id="estado" name="uf" class="form-control">
                  <option value="AC">Acre</option>
                  <option value="AL">Alagoas</option>
                  <option value="AP">Amapá</option>
                  <option value="AM">Amazonas</option>
                  <option value="BA">Bahia</option>
                  <option value="CE">Ceará</option>
                  <option value="DF">Distrito Federal</option>
                  <option value="ES">Espírito Santo</option>
                  <option value="GO">Goiás</option>
                  <option value="MA">Maranhão</option>
                  <option value="MT">Mato Grosso</option>
                  <option value="MS">Mato Grosso do Sul</option>
                  <option value="MG">Minas Gerais</option>
                  <option value="PA">Pará</option>
                  <option value="PB">Paraíba</option>
                  <option value="PR">Paraná</option>
                  <option value="PE">Pernambuco</option>
                  <option value="PI">Piauí</option>
                  <option value="RJ">Rio de Janeiro</option>
                  <option value="RN">Rio Grande do Norte</option>
                  <option value="RS">Rio Grande do Sul</option>
                  <option value="RO">Rondônia</option>
                  <option value="RR">Roraima</option>
                  <option value="SC">Santa Catarina</option>
                  <option value="SP">São Paulo</option>
                  <option value="SE">Sergipe</option>
                  <option value="TO">Tocantins</option>

              </select>
        </div>

        <div class="form-group col-xs-4">
            <label>Cidade:</label>
            <input type="text" class="form-control" id="cidade" name="cidade"  value="" placeholder="Cidade">
        </div>
        <div class="form-group col-xs-4">
            <label>Complemento:</label>
            <input type="text" class="form-control" id="complemento" name="complemento" value="" placeholder="Complemento">
        </div>

  </div>

    <div >
            <div class="col-lg-4 col-xs-6">
              <button type="button" id="btn-limpar" class="btn bg-orange btn-flat form-control ">Limpar</button>
            </div>

            <div class="col-lg-4 col-xs-6">
                <button type="submit" id="btnSubmit"  class="btn bg-green btn-flat  form-control">Salvar</button>
            </div>
    </div>

</form>
