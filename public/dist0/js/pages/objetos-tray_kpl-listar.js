$(document).ready(function() {

  var table =  $('#table-listagem-pedidos').DataTable({
       "processing": true,
       "serverSide": true,
       "language": {
                "url": "https://cdn.datatables.net/plug-ins/1.10.12/i18n/Portuguese-Brasil.json"
            },
       "ajax": {
           "url": $('#table-listagem-pedidos').attr('action'),
           "type": "POST",
           "data" :{ '_token':$('input[name="_token"]').val() ,'dataIni' : $('#dataIni').val() ,'dataFin' : $('#dataFin').val()  }
       },
       "columns": [

           { "data": "id" },
           { "data": "numero" },
           { "data": "tray_id" },
           { "data": "data" },
           { "data": "valor" },
           { "data": "frete_pago" },
           { "data": "frete_tray" },
           { "data": "destinatario" },
           { "data": "error" },
           { "data" : 'btn_reenvio'},

       ]

   });



  $('body').on( 'click' , '.btn-reenviopedido-tray' , function(){

        $idPedido = $(this).attr('codigoPed');
        $.ajax({
              type: 'POST' ,
              url: $('#form-reeenvio-pedido').val()+'/'+$idPedido ,
              data:  { '_token' : $('input[name="_token"]').val() } ,
          beforeSend: function( xhr ) {


           },
           success: function(retorno) {
             $('#error'+$idPedido).html('');
             $.bootstrapGrowl( retorno.msg ,{
                type: retorno.type ,
                delay: 4000,
             });
           }
        });
  })

});
