$(document).ready(function(){


            $('#box-grafico').prepend('<div class="overlay"><i class="fa fa-refresh fa-spin"></i></div>');


        var data=[];
        var colors=[];
        var Header=  ['Dias' , 'Entregues antecipado' , 'Não entregues'  , 'Entregues com atraso' , 'Entregues no dia'];
        data.push(Header);
      //  alert($('#DadosGraficos').val());


            $.each($.parseJSON($('#DadosGraficos').val()) , function(i, retorno) {


               var temp=[];
                temp.push(retorno.dataprevista);
                temp.push(parseInt(retorno.antecipado)  );
                temp.push(parseInt(retorno.atraso));
                temp.push(parseInt(retorno.atraso)  );
                temp.push(parseInt(retorno.dia));
                data.push(temp);
              });


          google.charts.load('current', {'packages':['corechart']});
          google.charts.setOnLoadCallback(drawVisualization(data));

          function drawVisualization(data) {
            // Some raw data (not necessarily accurate)
            var data = google.visualization.arrayToDataTable(data);

            var options = {
              title : 'Relatório de entrega diária',
              seriesType: 'bars',
              series: {4: {type: 'line'}}
            };

          var chart = new google.visualization.ComboChart(document.getElementById('chart_div'));
          chart.draw(data, options);
          }

          $('#box-grafico .overlay').remove();




    $('#table-entrega-diaria').dataTable(
      {
      "language": {
              "url": "https://cdn.datatables.net/plug-ins/1.10.12/i18n/Portuguese-Brasil.json"
          }
      });
})
