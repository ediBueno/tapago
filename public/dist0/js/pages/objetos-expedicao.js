$(document).ready(function() {


    function format ( d ) {
      // `d` is the original data object for the row
      var $subtabela =  '<table cellpadding="10" class="table" cellspacing="0" border="0" >'+
                    '<tr >'+
                        '<th colspan="7" >Dados do destinatário:</th>'+
                    '</tr>'+
                      '<tr>'+

                          '<th>Cpf/cnpj</th>'+
                          '<th colspan="6">Endereço</th>'+
                      '</tr>'+
                      '<tr>'+
                          '<td >'+d.cpf_cnpj+'</td>'+
                          '<td colspan="6">'+d.endereco_destino+'</td>'+

                      '</tr>'+
                      '<tr >'+
                          '<th colspan="7" >Dados da nota:</th>'+
                      '</tr>'+
                        '<tr>'+
                            '<th >Serie</th>'+
                            '<th>Chave</th>'+
                            '<th colspan="5" >Numero</th>'+
                        '</tr>'+
                        '<tr>'+
                            '<td>'+(d.serie_nf != null && d.serie_nf != ''  ? d.serie_nf : 'S/N' ) +'</td>'+
                            '<td>'+(d.key_nf != null && d.key_nf != '' ? d.key_nf : 'S/N' ) +'</td>'+
                              '<td  colspan="5">'+(d.numero_nf != null && d.numero_nf != '' ? d.numero_nf : 'S/N' ) +'</td>'+
                        '</tr>'+
                        '<tr>'+
                            '<th colspan="7" >Produtos:</th>'+
                        '</tr>'+
                          '<tr>'+
                              '<th>Código</th>'+
                              '<th>Número</th>'+
                              '<th>Descrição</th>'+
                              '<th>Comprimento</th>'+
                              '<th>Largura</th>'+
                              '<th>Altura</th>'+
                              '<th>Quantidade</th>'+
                          '</tr>' ;

                      $.each(d.produtos , function( key, value ) {

                            $subtabela +=  '<tr>'+
                                              '<td>'+value.id+'</td>'+
                                              '<td>'+value.codigo_externo+'</td>'+
                                              '<td>'+value.nome+'</td>'+
                                              '<td>'+value.comprimento+'</td>'+
                                              '<td>'+value.largura+'</td>'+
                                              '<td>'+value.altura+'</td>'+
                                              '<td>'+value.pivot.quantidade+'</td>'+
                                          '</tr>';
                        });

              $subtabela +=  '<tr>'+
                                  '<th colspan="7"  >Rastreio</th>'+
                              '</tr>'+
                              '<tr>'+
                                  '<td colspan="7" >'+d.codigo_rastreio+'</td>'+
                              '</tr>';

                      $subtabela +=  '</table>';

              return $subtabela ;
    }


  var table =  $('#table-listagem-pedidos').DataTable({
       "processing": true,
       "serverSide": true,
       "language": {
                "url": "https://cdn.datatables.net/plug-ins/1.10.12/i18n/Portuguese-Brasil.json"
            },
            "fnDrawCallback": function (oSettings) {
              $('input[type="checkbox"].checkImportArq').iCheck({
                    checkboxClass: 'icheckbox_minimal-blue',
                    radioClass   : 'iradio_minimal-blue'
              });

              $('#selectAllCheck').on('ifChecked', function (event){
                  $("input[name='arquivosImportDb']:checkbox").iCheck('check');

              });
              $('#selectAllCheck').on('ifUnchecked', function (event) {
                    $("input[name='arquivosImportDb']:checkbox").iCheck('uncheck');
              });
            } ,
            "ajax": {
                   "url": $('#table-listagem-pedidos').attr('action'),
                   "type": "POST",
                   "data" : function ( d ) {
                     return $.extend( {}, d,
                   { '_token':$('input[name="_token"]').val() ,'dataIni' : $('#dataIni').val()
                   ,'dataFin' : $('#dataFin').val(), 'destinatario' : $('#destinatario').val(), 'codigo_kpl' : $('#codigo_kpl').val(),
                    'data_postagem' : $('#data_postagem').val() ,'transportadora' : $('#idBaseTrans').val() ,'cep_destino' : $('#cep_destino').val()  }
                     );
                }
        },
       "columns": [
            { "data": "check"  ,  "orderable":      false },
           {
               "className":      'details-control',
               "orderable":      false,
               "data":           null,
               "defaultContent": ''
           },

           { "data": "id" },
           { "data": "numero" },
           { "data": "data" },
           { "data": "valor" },
           { "data": "frete_pago" },
           { "data": "destinatario" },
           { "data" : 'btn_reenvio'},

       ]

   });


   $('#btnExpedirLote').click(function(){

             $("input[name='arquivosImportDb']:checkbox:checked").each(function(){


                 var $this = $(this);
                     $.ajax({
                         url: $this.val() ,
                         dataType:'json',
                         type: "GET",
                         data:{ '_token' : $('input[name="_token"]').val() ,'lote' : 'true' },
                         beforeSend:function(){

                               $this.parent('div').parent('td').parent('tr')
                                                 .find('td')
                                                 .last().children('.textAdverte').remove();


                                 $this.parent('div').parent('td').parent('tr')
                                                 .find('td')
                                                 .last().text('');

                                 $this.parent('div').parent('td').parent('tr')
                                 .find('td')
                                 .last()
                                 .append('<div style="min-width:100px;" class="progress progress-sm active">'+
                                           '<div class="progress-bar progress-bar-success progress-bar-striped" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" >'+
                                             '<span class="sr-only">20% Complete</span>'+
                                           '</div>'+
                                         '</div>');


                                 var progressBar = $this.parent('div').parent('td').parent('tr')
                                                   .find('td')
                                                   .last().children('.progress').children('.progress-bar');

                                 setInterval(function() {

                                   progressBar.css('width' , parseInt(progressBar.width()) + 15 + "%" );

                                 }, 1000);


                         },
                         success: function(data){

                           $this.parent('div').parent('td').parent('tr')
                                             .find('td')
                                             .last().children('.progress').remove();

                           $this.parent('div').parent('td').parent('tr')
                                             .find('td')
                                             .last().css('width' ,'150px');


                             $this.parent('div').parent('td').parent('tr')
                                               .find('td')
                                               .last().append('<span class="text-danger textAdverte" >&nbsp;&nbsp;&nbsp;&nbsp;'+data.msg+'</span>');



                                               if (data.success == true ) {

                                                    $this.parent('div').parent('td').parent('tr').fadeOut(3000);
                                               }
                        },
                        error :function(){


                        }


                     });

                   });

   });

   $('input[type="checkbox"].checkImportArq').iCheck({
         checkboxClass: 'icheckbox_minimal-blue',
         radioClass   : 'iradio_minimal-blue'
   });

   $('#table-listagem-pedidos tbody').on('click', 'td.details-control', function () {
       var tr = $(this).closest('tr');
       var row = table.row( tr );

       if ( row.child.isShown() ) {
           // This row is already open - close it
           row.child.hide();
           tr.removeClass('shown');
       }
       else {
           // Open this row
           row.child( format(row.data()) ).show();
           tr.addClass('shown');
       }
   });

   $('input[type="checkbox"].arquivosCotarCte').iCheck({
     checkboxClass: 'icheckbox_minimal-blue',
     radioClass   : 'iradio_minimal-blue'
   });


   $('#btnSubmit').click(function() {

       $('#table-listagem-pedidos').DataTable().draw();
   });

   $('#btn-limpar').click(function() {
       $('#form-busca').each(function () {
           this.reset();
        });
    });


  $('body').on( 'click' , '.btn-reenviopedido-tray' , function(){

        $idPedido = $(this).attr('codigoPed');
        $.ajax({
              type: 'POST' ,
              url: $('#form-reeenvio-pedido').val()+'/'+$idPedido ,
              data:  { '_token' : $('input[name="_token"]').val() } ,
          beforeSend: function( xhr ) {


           },
           success: function(retorno) {
             $('#error'+$idPedido).html('');
             $.bootstrapGrowl( retorno.msg ,{
                type: retorno.type ,
                delay: 4000,
             });
           }
        });
  })

});

$(function () {


  $('.dataIniFim').daterangepicker({
       autoUpdateInput: false,
       locale: {

           "format": "DD/MM/YYYY",

           "separator": " - ",
           "applyLabel": "Aplicar",
           "cancelLabel": "Cancelar",
           "fromLabel": "From",
           "toLabel": "To",
           "customRangeLabel": "Custom",
           "daysOfWeek": [
               "Do",
               "Se",
               "Te",
               "Qu",
               "Qu",
               "Se",
               "Sa"
           ],
           "monthNames": [
               "Janeiro",
               "Fevereiro",
               "Março",
               "Abril",
               "Maio",
               "Junho",
               "Julho",
               "Agosto",
               "Setembro",
               "Outubro",
               "Novembro",
               "Dezembro"
           ],
       }
   });


   $('.dataIniFim').on('apply.daterangepicker', function(ev, picker) {
     $(this).val(picker.startDate.format('DD/MM/YYYY') + ' - ' + picker.endDate.format('DD/MM/YYYY'));
 });

 $('.dataIniFim').on('cancel.daterangepicker', function(ev, picker) {
     $(this).val('');
 });




  })
