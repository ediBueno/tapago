
$(function () {
$('.dataSimples').datepicker({      dateFormat: 'dd/mm/yy', locale: 'pt-br'  });
});
$(document).ready(function(){
        //Date picker
   $('[data-mask]').inputmask();

  $('body').on('click' , '.btn-analise-reaberto' , function(){

      /*Retirada a obrigatoriedade de mensagem prq o Claudemir disse que não é necessario */
       if($('#descricao-reabertura').val() == '' && $(this).attr('opcao-finaliza') != 'pagamento'){
          $('#div-descricao').addClass('has-error');
          $('#div-descricao .help-block').remove();
          $('#div-descricao').append('<span class="help-block">Faça uma breve descrição!</span>');
          return false;
       }else {
           $codigoRemove = $(this).attr('codigo');
           $.ajax({
               url: $('#route-reabertura').val()+"/"+$(this).attr('codigo')+"" ,
              dataType:'json',
              type: "POST",
               data:{ '_token': $('input[name="_token"]').val() , finalisar : $(this).attr('opcao-finaliza')
                      ,'descricao' : $('#descricao-reabertura').val() ,'totalDiasPrazo' : $('input[name="totalDiasPrazo"]').val()},
               success: function(retorno){

                 if ($('#pagEmails').val() == 'true') {

                     $('#modal-default').modal('hide');
                     $('#tr'+$codigoRemove).parent('td').parent('tr').remove();

                 }


                   $.bootstrapGrowl( retorno.msg ,{
                      type: retorno.type ,
                      delay: 4000,
                   });

                   $body = '';
                   $.each(retorno.dados.reativacoes, function(i, item) {
                       $body +=  '<tr><td>'+item.created_at+'</td> <td>'+item.usuario.name+'</td> <td>'+item.descricao+'</td></tr>';
                   });

                   $('#table-listar-reativacoes tbody').html($body);
                   $('#descricao-reabertura').val('');

               }
             })

      }



  });

  $('body').on('click' , '#btn-mensagem-analise' , function(){
    $('#descricao-reabertura').val('Analisado abertura indevida');
  });

  $('body').on('click' , '#btn-reabrir' , function(){

      if ($('#descricao-reabertura').val() == '') {
        alert('Digite um texto referente a reabertura!');
        $('#descricao-reabertura').focus();
      }else{

            $('#descricao-reabertura').focus();
            $('#descricao-reabertura').select();
            document.execCommand("copy");
            window.open($(this).attr('data-href'));
      }

  });


    $('body').on('click' , '#btn-reabrir-obj1' , function(){
        $('#descricao-reabertura').focus();
        $('#descricao-reabertura').val('Prezado Correios por favor indenizar por atraso sendo que objeto em questão deveria ter ocorrido a entrega a domicilio de acordo com o calculador de preços e prazos, sendo que não ocorreu tentativa de entrega no local indicado na postagem, além de ocorrer o não prestação de serviço de entrega prevista em contrato.');
        $('#descricao-reabertura').select();
        document.execCommand("copy");

        window.open($(this).attr('data-href'));

    });

  $('body').on('click' , '#btn-reabrir-obj2' , function(){
      $('#descricao-reabertura').focus();
      $('#descricao-reabertura').val('Prezados Correios conforme calculador de preços e prazos na data da postagem foi informado que a entrega seria em domicilio, sendo que entrega via posta restante não faz jus ao serviço contratado devendo ter sido entregue em domicilio conforme consta em contrato mediante ao fato favor indenizar o atraso e serviço de entrega a domicilio não prestado.');
      $('#descricao-reabertura').select();
      document.execCommand("copy");

      window.open($(this).attr('data-href'));

  });

  $('body').on('click' , '#btn-reabrir-obj3' , function(){
      $('#descricao-reabertura').focus();
      $('#descricao-reabertura').val('Prezado Correios por favor indenizar por atraso sendo que objeto em questão deveria ter ocorrido a entrega a domicilio de acordo com o calculador de preços e prazos, sendo que não ocorreu tentativa de entrega no local indicado na postagem, além de ocorrer o não prestação de serviço de entrega prevista em contrato.');
      $('#descricao-reabertura').select();
      document.execCommand("copy");

      window.open($(this).attr('data-href'));

  });




})
