 google.charts.load('current', {'packages':['corechart']});
   google.charts.setOnLoadCallback(drawChart);



                $('#svg-map a').hover(function(){

                    estado = $('#input-'+$(this).data('uf')).val();
                    jsonRetorno = JSON.parse(estado);
                    $('#box-detalhe-estado .info-box-text').html(jsonRetorno.nome);
                    $('#box-detalhe-estado .info-box-number').html(jsonRetorno.totalEstado+' envios');
                    $('#box-detalhe-estado .progress-bar').css('width' , jsonRetorno.porcent+'%'  );

                    $('#box-detalhe-estado .progress-description').html(jsonRetorno.porcent+'%'+' do total enviado');

                    $('#box-detalhe-estado-enviados .info-box-number').html(jsonRetorno.totalEnviados);
                    $('#box-detalhe-estado-enviados .progress-bar').css('width' , jsonRetorno.porcentEnviados+'%'  );
                    $('#box-detalhe-estado-enviados .progress-description').html(jsonRetorno.porcentEnviados+'%'+' do total do estado');

                    $('#box-atraso .description-total span').text(jsonRetorno.totalObjComAtraso);

                  //  console.log(jsonRetorno.servicos);

                    $table = '<table class="table" ><thead><th>Serviço</th><th>Méd dias sol</th><th>Méd dias entrega</th><th>Méd atraso<th>Total</th><thead><tbody>';
                    $.each(jsonRetorno.servicos , function (key, data) {



                        $table += '<tr><td>'+data.nome+'</td><td>'+data.mediaTotalDiasUteisSolicitados+'</td><td>'+data.mediaTotalDiasUteisEntrega+'</td><td>'+data.mediaTotalDiasUteisAtraso+'</td><td>'+data.total+'</td></tr>';

                    })

                    $table += '</tbody></table>';

                    $('#painel-detalhe-servicos .box-body').html($table );

                    $('#painel-detalhe-estado').removeClass('hide');
                });


    //Date picker
   $('.buscaData').datepicker({
     autoclose: true,
     language:'pt-BR'
   });


   $('.alt-default-sistema').click(function(){
      if ($(this).val() == 1) {
         $('#alt-default-sistema_0').prop('checked' ,false);
      }else {
         $('#alt-default-sistema_1').prop('checked' ,false);
      }


     if($(this).is(':checked')){
        $.ajax({
         url: $(this).attr('action'),
         type:'POST',
         data: {_token : $('#form-config-user input[name="_token_config"]').val() ,sistema_default : $(this).val() },
         success: function(retorno) {

           if(retorno.sistema_default == 1){
               $('a[is=0]').click();
           }else{
                 $('a[is=1]').click();
           }

         }
       });
     }
   });


   function drawChart() {
        var data=[];
        var colors=[];
        var Header= ['Estado', 'Total'];
        data.push(Header);

             $("input[id^='input-']").each(function(key ,val){
                     retorno = JSON.parse($(val).val());
                      var temp=[];
                      temp.push(retorno.nome);
                      temp.push(retorno.totalEstado);
                      data.push(temp);
                      colors.push(retorno.COR);
               })
                var optionsPie = {
                  chartArea:{
                      left:10,
                      right:10, // !!! works !!!
                      bottom:1,  // !!! works !!!
                      top:1,
                  },
                colors: colors ,
                title: 'My Daily Activities'
              };
              var data = google.visualization.arrayToDataTable(data);
              var chart = new google.visualization.PieChart(document.getElementById('piechart'));

              chart.draw(data, optionsPie);


              /********************************************************************************************************************************/
              function graficoPizza(dados ,idgrafico ){
                    var dataTotal = new google.visualization.DataTable();
                    dataTotal.addColumn('string', 'Topping');
                    dataTotal.addColumn('number', 'Slices');
                    dataTotal.addRows(
                    dados
                    );
                    var options = {
                    chartArea:{
                        left:10,
                        right:10, // !!! works !!!
                        bottom:1,  // !!! works !!!
                        top:1,
                    },
                    legend:'none',
                    width:"100",
                    height:"100",
                    'backgroundColor': 'transparent',
                    'is3D':true,
                  };

                    var chart = new google.visualization.PieChart(document.getElementById(idgrafico));
                    chart.draw(dataTotal, options);
              }

              /********************************************************************************************************************************/


              dados = [];
              dadosTemp = [];
              dadosTemp.push('Entregues');
              dadosTemp.push(parseInt($('#entregues').text()));

             dados.push(dadosTemp);
             dadosTemp = [];
             dadosTemp.push('Abertos');
             dadosTemp.push(parseInt($('#abertos').text()));
             dados.push(dadosTemp);

            graficoPizza(dados ,'grafico-entregue-abertos');

            dados = [];
            dadosTemp = [];
            dadosTemp.push('Normal');
            dadosTemp.push(parseInt($('#normal').text()));

             dados.push(dadosTemp);
             dadosTemp = [];
             dadosTemp.push('Atraso');
             dadosTemp.push(parseInt($('#atraso').text()));
            dados.push(dadosTemp);

            graficoPizza(dados ,'grafico-normal-atraso');

            dados = [];

             dadosTemp = [];
             dadosTemp.push('Notificados');
             dadosTemp.push(parseInt($('#notificados').text()));
            dados.push(dadosTemp);

            dadosTemp = [];
            dadosTemp.push('Não Notificados');
            dadosTemp.push(parseInt($('#atraso').text()) - parseInt($('#notificados').text()));

            dados.push(dadosTemp);

            graficoPizza(dados ,'grafico-notificados');
            }
$(document).ready(function(){

$('select[name="cliente"]').on('change' ,function(){


    $form = '<form  action="'+$(this).parent('.link').attr('action')+'" method="POST" id="form-update-user-cliente" >'+
                                                        '<input type="hidden" name="cliente_id" value="'+$(this).val()+'" >'+
                                                        '</form>';
    $('#form-update-user-cliente').remove();
    $('body').prepend($form);
   $('#form-update-user-cliente').submit();

})
})
