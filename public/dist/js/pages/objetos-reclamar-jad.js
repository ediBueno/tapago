
function format ( d ) {


    return '<table cellpadding="10" class="table" cellspacing="0" border="0" >'+
                '<tr>'+
                    '<th>Ent domic&iacute;lio:</th>'+
                    '<th>Posição</th>'+
                    '<th>Reclamação</th>'+
                '</tr>'+
                '<tr>'+
                    '<td>'+d.entregadomiciliar+'</td>'+
                    '<td>'+d.posicaodoobjeto+'</td>'+
                    '<td>'+(d.abrirreclamacao != null ? d.abrirreclamacao : '-' )+'</td>'+
                '</tr>'+
            '</table>';


}

function modalMovimentacoesRastreio(url , numero) {

  $.ajax({
      url: url ,
     dataType:'json',
      data:{ '_token':$('input[name="_token"]').val() },
      success: function(data){

      $lista =   '<ul class="timeline">';
        $.each(data.rastreios, function (key, dados) {

          if(dados.tipo == 'BDE'  ) {
            icone = 'fa-home' ;
          }else {
            icone =  'fa-check-square-o'   ;
          }
          $lista +=     '<li class="time-label">'+
                            '<span class="bg-red">'+
                              dados.data+
                            '</span>'+
                        '</li>'+
                        '<li>'+
                        '<i class="fa ' + icone +' bg-blue"></i>'+
                          '<div class="timeline-item">'+
                            '<span class="time"><i class="fa fa-clock-o"></i>'+dados.hora+'</span>'+
                            '<h3 class="timeline-header"><a href="#">'+dados.descricao+'</a> TIPO : '+dados.tipo+'      STATUS : '+dados.status+'</h3>'+
                          '</div>'+
                      '</li>';
              });

              $lista +=    '</ul>';


          $('#modal-default .modal-header h4').text('Cod Registro: '+numero);
          $('#modal-default .modal-body').html($lista);
          $('#modal-default .modal-footer').remove();
          $('#modal-default').modal();
      }
  });

}

function modalEmails(url , numero) {

  $.ajax({
      url: url ,
     dataType:'json',
      data:{ '_token':$('input[name="_token"]').val() },
      success: function(data){

          $('#modal-default .modal-header h4').text('Cod Registro: '+numero);
          $('#modal-default .modal-body').html(data.emails.timeline);
          $('#modal-default .modal-footer').remove();
          $('#modal-default').modal();
      }
  });

}

//console.log(document.getElementById('table-listagem-objetos_filter'));

//.children('input').placeholder = 'Novo tye';

$(document).ready(function() {


  var table =  $('#table-listagem-objetos').DataTable({
       "processing": true,
       "serverSide": true,
       "language": {
                "url": "https://cdn.datatables.net/plug-ins/1.10.12/i18n/Portuguese-Brasil.json",

            },

       "ajax": {
          "url": $('#table-listagem-objetos').attr('action')+'/'+$('#pagTipo').val(),
           "type": "POST",
           "data" :  function ( d ) {
             return $.extend( {}, d,
               { '_token':$('input[name="_token"]').val()
                         ,'estado' :$('#buscar-por-estado').val()
                         ,'criticos' : $('#pagCriticos').val()
                         ,'mod' : $('#mod').val()
                         ,'dataIni' : $('#dataIni').val() ,'dataFin' : $('#dataFin').val()
                         ,'buscaEmails' : 'false'
                          ,'statusBuscar' : $('#status-buscarcorreios option:selected').val()
                         ,'cep_origem' : $('#cep_origemcorreios').val()
                         ,'cep_destino' : $('#cep_destinocorreios').val()
                         ,'destinatario' : $('#destinatariocorreios').val()
                         ,'data_postagem' : $('#data_postagemcorreios').val()
                         ,'data_considerada' :$('#data_consideradacorreios').val()
                         ,'data_prevista' :$('#data_previstacorreios').val()
                         ,'data_entrega' :$('#data_entregacorreios').val()
                         ,'protocolo' : $('#protocolo').val()                      
                         ,'clientes_id' :  $('select[name="cliente"]').val()
                       }
                         );
                    }
                  }
           ,
       "columns": [
            {
                "className":      'details-control',
                "orderable":      false,
                "data":           null,
                "defaultContent": ''
            },
           { "data": "id" },
           { "data": "codrastreio" },
           { "data": "qtdDiasPrevEntregaUteis" },
           { "data": "dataconsiderada" },
           { "data": "dataprevista" },
           { "data": "dataentrega" },
           { "data": "ceporigem" ,"orderable":      false},
           { "data": "cepdestino" ,"orderable":      false},
           { "data": "statusenvio" },
           { "data": "codigoreclamacao" },
          // { "data": "enviou_xml" },
           {"data" : 'btnBetalhe' ,"orderable":      false }
       ]

   });

   $('#btnSubmitcorreios').click(function() {

       $('#table-listagem-objetos').DataTable().draw();
   });

   $('#btn-limparcorreios').click(function() {
       $('#form-buscacorreios').each(function () {
           this.reset();
        });
    });





// #myInput is a <input type="text"> element

   // Add event listener for opening and closing details
      $('#table-listagem-objetos tbody').on('click', 'td.details-control', function () {
          var tr = $(this).closest('tr');
          var row = table.row( tr );

          if ( row.child.isShown() ) {
              // This row is already open - close it
              row.child.hide();
              tr.removeClass('shown');
          }
          else {
              // Open this row
              row.child( format(row.data()) ).show();
              tr.addClass('shown');
          }
      });

      $('#btn-busca-periodo').on('click' ,function(event){
          event.preventDefault();
           $('#form-busca-periodo').submit() ;
           $('input[name="pg"]').val( parseInt($('input[name="pg"]').val( )) + 1 );

      });

      $('.buscaData').on('change' ,function(event){
        $.ajax({
              type:"POST",
             url:   $('#form-busca-periodo').attr('action') ,
             dataType:'json',
             data:{ pg : '-1' ,'_token':$('input[name="_token"]').val() ,dataIni : $('input[name="dataIni"]').val() ,dataFim : $('input[name="dataFim"]').val() },
             success: function(data){
                $('#totalPaginas').text(data.totalPaginas);
                $('#totalLinhas').text(data.t);
            }
        });
      });

});


$(function () {


  $('.dataIniFim').daterangepicker({
       autoUpdateInput: false,
       locale: {
           "format": "DD/MM/YYYY",
           "separator": " - ",
           "applyLabel": "Aplicar",
           "cancelLabel": "Cancelar",
           "fromLabel": "From",
           "toLabel": "To",
           "customRangeLabel": "Custom",
           "daysOfWeek": [
               "Do",
               "Se",
               "Te",
               "Qu",
               "Qu",
               "Se",
               "Sa"
           ],
           "monthNames": [
               "Janeiro",
               "Fevereiro",
               "Março",
               "Abril",
               "Maio",
               "Junho",
               "Julho",
               "Agosto",
               "Setembro",
               "Outubro",
               "Novembro",
               "Dezembro"
           ],
       }
   });


   $('.dataIniFim').on('apply.daterangepicker', function(ev, picker) {
     $(this).val(picker.startDate.format('DD/MM/YYYY') + ' - ' + picker.endDate.format('DD/MM/YYYY'));
 });

 $('.dataIniFim').on('cancel.daterangepicker', function(ev, picker) {
     $(this).val('');
 });




  })
