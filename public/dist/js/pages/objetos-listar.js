
function format ( d ) {
    // `d` is the original data object for the row
  if($('#pagCriticos').val()){ //se for a listagem de objetos criticos

    return '<table cellpadding="10" class="table" cellspacing="0" border="0" >'+
                '<tr>'+
                    '<th>Dt. Cons:</th>'+
                    '<th>Cep Orig</th>'+
                    '<th>Cep Dest</th>'+
                    '<th>Reclamação</th>'+
                '</tr>'+
                '<tr>'+
                    '<td>'+d.dataconsiderada+'</td>'+
                    '<td>'+d.ceporigem+'</td>'+
                      '<td>'+d.cepdestino+'</td>'+
                    '<td>'+(d.abrirreclamacao != null ? d.abrirreclamacao : '-' )+'</td>'+
                '</tr>'+
                '<tr>'+
                    '<th>Comprimento:</th>'+
                    '<th>Largura</th>'+
                    '<th>Altura </th>'+
                    '<th>Peso</th>'+
                '</tr>'+
                '<tr>'+
                    '<td>'+d.comprimento+'</td>'+
                    '<td>'+d.largura+'</td>'+
                      '<td>'+d.altura+'</td>'+
                    '<td>'+ d.peso +'</td>'+
                '</tr>'+
            '</table>';
          }else{

    return '<table cellpadding="10" class="table" cellspacing="0" border="0" >'+
                '<tr>'+
                    '<th>Ent domic&iacute;lio:</th>'+
                    '<th>Posição</th>'+
                    '<th>Reclamação</th>'+
                '</tr>'+
                '<tr>'+
                    '<td>'+d.entregadomiciliar+'</td>'+
                    '<td>'+d.posicaodoobjeto+'</td>'+
                    '<td>'+(d.abrirreclamacao != null ? d.abrirreclamacao : '-' )+'</td>'+
                '</tr>'+
                '<tr>'+
                    '<th>Comprimento:</th>'+
                    '<th>Largura</th>'+
                    '<th>Altura </th>'+
                    '<th>Peso</th>'+
                '</tr>'+
                '<tr>'+
                    '<td>'+d.comprimento+'</td>'+
                    '<td>'+d.largura+'</td>'+
                      '<td>'+d.altura+'</td>'+
                    '<td>'+ d.peso +'</td>'+
                '</tr>'+
            '</table>';

          }
}

function modalMovimentacoesRastreio(url , numero) {

  $.ajax({
      url: url ,
     dataType:'json',
      data:{ '_token':$('input[name="_token"]').val() },
      success: function(data){

      $lista =   '<ul class="timeline">';
        $.each(data.rastreios, function (key, dados) {

          if(dados.tipo == 'BDE'  ) {
            icone = 'fa-home' ;
          }else {
            icone =  'fa-check-square-o'   ;
          }
          $lista +=     '<li class="time-label">'+
                            '<span class="bg-red">'+
                              dados.data+
                            '</span>'+
                        '</li>'+
                        '<li>'+
                        '<i class="fa ' + icone +' bg-blue"></i>'+
                          '<div class="timeline-item">'+
                            '<span class="time"><i class="fa fa-clock-o"></i>'+dados.hora+'</span>'+
                            '<h3 class="timeline-header"><a href="#">'+dados.descricao+'</a> TIPO : '+dados.tipo+'      STATUS : '+dados.status+'</h3>'+
                          '</div>'+
                      '</li>';
              });

              $lista +=    '</ul>';


          $('#modal-default .modal-header h4').text('Cod Registro: '+numero);
          $('#modal-default .modal-body').html($lista);
          $('#modal-default .modal-footer').remove();
          $('#modal-default').modal();
      }
  });

}

function modalEmails(url , numero) {

  $.ajax({
      url: url ,
     dataType:'json',
      data:{ '_token':$('input[name="_token"]').val() },
      success: function(data){

          $('#modal-default .modal-header h4').text('Cod Registro: '+numero);
          $('#modal-default .modal-body').html(data.emails.timeline);
          $('#modal-default .modal-footer').remove();
          $('#modal-default').modal();
      }
  });

}

//console.log(document.getElementById('table-listagem-objetos_filter'));

//.children('input').placeholder = 'Novo tye';

$(document).ready(function() {



if($('#pagEmails').val()){

$('#table-listagem-objetos thead').html('<tr>'+
                                              '<th></th>'+
                                              '<th>#</th>'+
                                              '<th>Cod Reg</th>'+
                                              '<th>PI</th>'+
                                              '<th>Data ultima analise</th>'+
                                              '<th>Data ultimo email</th>'+
                                              '<th>Data Prazo</th>'+
                                              '<th>Status Resposta</th>'+
                                              '<th>Status</th>'+
                                              '<th>Analisar</th>'+
                                              '<th></th>'+
                                          '</tr>');

  var table =  $('#table-listagem-objetos').DataTable({
       "processing": true,
       "serverSide": true,
       "language": {
                "url": "https://cdn.datatables.net/plug-ins/1.10.12/i18n/Portuguese-Brasil.json"
            },
       "ajax": {
           "url": $('#table-listagem-objetos').attr('action')+'/'+$('#pagTipo').val(),
           "type": "POST",
           "data" : function ( d ) {
             return $.extend( {}, d,
               { '_token':$('input[name="_token"]').val() ,
                      'estado' :$('#buscar-por-estado').val() ,'criticos' : $('#pagCriticos').val()
                        ,'statusBuscar' : $('#status-buscaremails option:selected').val()
                        , 'statusResposta' : $('#status-respostaemails option:selected').val()
                        ,'cep_origem' : $('#cep_origememails').val()
                        ,'cep_destino' : $('#cep_destinoemails').val()
                        ,'destinatario' : $('#destinatarioemails').val()
                        ,'data_postagem' : $('#data_postagememails').val()
                        ,'data_ultanalise' :$('#data_ultanalise').val()
                        ,'data_ultemail' :$('#data_ultemail').val()
                        ,'data_prazo' :$('#data_prazo').val()
                       ,'dataIni' : $('#dataIni').val() ,'dataFin' : $('#dataFin').val()
                      ,'buscaEmails' : 'true' ,'clientes_id' :  $('select[name="cliente"]').val()  }
                    );
                    }

       },
       "columns": [
            {
                "className":      'details-control',
                "orderable":      false,
                "data":           null,
                "defaultContent": ''
            },
           { "data": "id" },
           { "data": "codrastreio" },
           { "data": "codigoreclamacao" },
           { "data": "ultima_analise" },
           { "data": "emails.data" },
           { "data": "data_prazo" },
           { "data": "tipo_email" },
           { "data": "statusenvio" },
           { "data" : 'btnAnalise' ,"orderable":      false },
           { "data" : 'btnBetalhe' ,"orderable":      false }
       ]

   });

   $('#btnSubmitemails').click(function() {

       $('#table-listagem-objetos').DataTable().draw();
   });

   $('#btn-limparemails').click(function() {
       $('#form-buscaemails').each(function () {
           this.reset();
        });
    });
}else{
  var table =  $('#table-listagem-objetos').DataTable({
       "processing": true,
       "serverSide": true,
       "language": {
                "url": "https://cdn.datatables.net/plug-ins/1.10.12/i18n/Portuguese-Brasil.json",

            },

       "ajax": {
           "url": $('#table-listagem-objetos').attr('action'),
           "type": "POST",
           "data" :  function ( d ) {
             return $.extend( {}, d,
               { '_token':$('input[name="_token"]').val()
                         ,'estado' :$('#buscar-por-estado').val()
                         ,'criticos' : $('#pagCriticos').val()
                         ,'mod' : $('#mod').val()
                         ,'dataIni' : $('#dataIni').val() ,'dataFin' : $('#dataFin').val()
                         ,'buscaEmails' : 'false'
                          ,'statusBuscar' : $('#status-buscarcorreios option:selected').val()
                         ,'cep_origem' : $('#cep_origemcorreios').val()
                         ,'cep_destino' : $('#cep_destinocorreios').val()
                         ,'destinatario' : $('#destinatariocorreios').val()
                         ,'data_postagem' : $('#data_postagemcorreios').val()
                         ,'data_considerada' :$('#data_consideradacorreios').val()
                         ,'data_prevista' :$('#data_previstacorreios').val()
                         ,'data_entrega' :$('#data_entregacorreios').val()
                         ,'protocolo' : $('#protocolo').val()
                         ,'comResposta' : $('#com-resposta').iCheck('update')[0].checked
                         ,'clientes_id' :  $('select[name="cliente"]').val()
                       }
                         );
                    }
                  }
           ,
       "columns": [
            {
                "className":      'details-control',
                "orderable":      false,
                "data":           null,
                "defaultContent": ''
            },
           { "data": "id" },
           { "data": "codrastreio" },
           { "data": "qtdDiasPrevEntregaUteis" },
           { "data": "dataconsiderada" },
           { "data": "dataprevista" },
           { "data": "dataentrega" },
           { "data": "ceporigem" ,"orderable":      false},
           { "data": "cepdestino" ,"orderable":      false},
           { "data": "statusenvio" },
           { "data": "codigoreclamacao" },
          // { "data": "enviou_xml" },
           {"data" : 'btnBetalhe' ,"orderable":      false }
       ]

   });

   $('#btnSubmitcorreios').click(function() {

       $('#table-listagem-objetos').DataTable().draw();
   });

   $('#btn-limparcorreios').click(function() {
       $('#form-buscacorreios').each(function () {
           this.reset();
        });
    });

}



// #myInput is a <input type="text"> element


   // Add event listener for opening and closing details
      $('#table-listagem-objetos tbody').on('click', 'td.details-control', function () {
          var tr = $(this).closest('tr');
          var row = table.row( tr );

          if ( row.child.isShown() ) {
              // This row is already open - close it
              row.child.hide();
              tr.removeClass('shown');
          }
          else {
              // Open this row
              row.child( format(row.data()) ).show();
              tr.addClass('shown');
          }
      });





      $('#btn-busca-periodo').on('click' ,function(event){


          event.preventDefault();


           $('#form-busca-periodo').submit() ;
           $('input[name="pg"]').val( parseInt($('input[name="pg"]').val( )) + 1 );

      });

      $('.buscaData').on('change' ,function(event){
        $.ajax({
              type:"POST",
             url:   $('#form-busca-periodo').attr('action') ,
             dataType:'json',
             data:{ pg : '-1' ,'_token':$('input[name="_token"]').val() ,dataIni : $('input[name="dataIni"]').val() ,dataFim : $('input[name="dataFim"]').val() },
             success: function(data){
                $('#totalPaginas').text(data.totalPaginas);
                $('#totalLinhas').text(data.t);
            }
        });
      });



/******************************************Busca jad**********/

var tableJad =  $('#table-listagem-objetos-jad').DataTable({
     "processing": true,
     "serverSide": true,
     "language": {
              "url": "https://cdn.datatables.net/plug-ins/1.10.12/i18n/Portuguese-Brasil.json"
          },
     "ajax": {
         "url": $('#table-listagem-objetos-jad').attr('action'),
         "type": "POST",
         "data" :  function ( d ) {
           return $.extend( {}, d,
             { '_token':$('input[name="_token"]').val()
                       ,'estado' :$('#buscar-por-estado').val()
                       ,'criticos' : $('#pagCriticos').val()
                       ,'mod' : $('#mod').val()
                       ,'dataIni' : $('#dataIni').val() ,'dataFin' : $('#dataFin').val()
                       ,'buscaEmails' : 'false'
                        ,'statusBuscar' : $('#status-buscarjad option:selected').val()
                       ,'cep_origem' : $('#cep_origemjad').val()
                       ,'cep_destino' : $('#cep_destinojad').val()
                       ,'destinatario' : $('#destinatariojad').val()
                       ,'data_postagem' : $('#data_postagemjad').val()
                       ,'data_considerada' :$('#data_consideradajad').val()
                       ,'data_prevista' :$('#data_previstajad').val()
                       ,'data_entrega' :$('#data_entregajad').val()
                       ,'protocolo' : $('#protocolo').val()
                       ,'comResposta' : $('#com-resposta').iCheck('update')[0].checked
                       ,'clientes_id' :  $('select[name="cliente"]').val()
                     }
                       );
                  }
     },
     "columns": [
          {
              "className":      'details-control',
              "orderable":      false,
              "data":           null,
              "defaultContent": ''
          },
         { "data": "id" },
         { "data": "codrastreio" },
         { "data": "qtdDiasPrevEntregaUteis" },
         { "data": "dataconsiderada" },
         { "data": "dataprevista" },
         { "data": "dataentrega" },
         { "data": "ceporigem" },
         { "data": "cepdestino" },
         { "data": "statusenvio" },
         { "data": "codigoreclamacao" },
        //   { "data": "enviou_xml" },
         {"data" : 'btnBetalhe' ,"orderable":      false,}
     ]

 });

 // Add event listener for opening and closing details
    $('#table-listagem-objetos-jad tbody').on('click', 'td.details-control', function () {
        var tr = $(this).closest('tr');
        var row = tableJad.row( tr );

        if ( row.child.isShown() ) {
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
        }
        else {
            // Open this row
            row.child( format(row.data()) ).show();
            tr.addClass('shown');
        }
    });

    $('#btnSubmitjad').click(function() {

        $('#table-listagem-objetos-jad').DataTable().draw();
    });

    $('#btn-limparjad').click(function() {
        $('#form-buscajad').each(function () {
            this.reset();
         });
     });

});

/**********************************************************************************/

/******************************************Busca euentrego****************************************************************************/

var tableJad =  $('#table-listagem-objetos-euentrego').DataTable({
     "processing": true,
     "serverSide": true,
     "language": {
              "url": "https://cdn.datatables.net/plug-ins/1.10.12/i18n/Portuguese-Brasil.json"
          },
     "ajax": {
         "url": $('#table-listagem-objetos-euentrego').attr('action'),
         "type": "POST",
         "data" :  function ( d ) {
           return $.extend( {}, d,
             { '_token':$('input[name="_token"]').val()
                       ,'estado' :$('#buscar-por-estado').val()
                       ,'criticos' : $('#pagCriticos').val()
                       ,'mod' : $('#mod').val()
                       ,'dataIni' : $('#dataIni').val() ,'dataFin' : $('#dataFin').val()
                       ,'buscaEmails' : 'false'
                        ,'statusBuscar' : $('#status-buscareuentrego option:selected').val()
                       ,'cep_origem' : $('#cep_origemeuentrego').val()
                       ,'cep_destino' : $('#cep_destinoeuentrego').val()
                       ,'destinatario' : $('#destinatarioeuentrego').val()
                       ,'data_postagem' : $('#data_postagemeuentrego').val()
                       ,'data_considerada' :$('#data_consideradaeuentrego').val()
                       ,'data_prevista' :$('#data_previstaeuentrego').val()
                       ,'data_entrega' :$('#data_entregaeuentrego').val()
                       ,'protocolo' : $('#protocolo').val()
                       ,'comResposta' : $('#com-resposta').iCheck('update')[0].checked
                       ,'clientes_id' :  $('select[name="cliente"]').val()
                     }
                       );
                  }
     },
     "columns": [
          {
              "className":      'details-control',
              "orderable":      false,
              "data":           null,
              "defaultContent": ''
          },
         { "data": "id" },
         { "data": "codrastreio" },
         { "data": "qtdDiasPrevEntregaUteis" },
         { "data": "dataconsiderada" },
         { "data": "dataprevista" },
         { "data": "dataentrega" },
         { "data": "ceporigem" },
         { "data": "cepdestino" },
         { "data": "statusenvio" },
         { "data": "codigoreclamacao" },
        //   { "data": "enviou_xml" },
         {"data" : 'btnBetalhe' ,"orderable":      false }
     ]

 });

 // Add event listener for opening and closing details
    $('#table-listagem-objetos-euentrego tbody').on('click', 'td.details-control', function () {
        var tr = $(this).closest('tr');
        var row = tableJad.row( tr );

        if ( row.child.isShown() ) {
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
        }
        else {
            // Open this row
            row.child( format(row.data()) ).show();
            tr.addClass('shown');
        }
    });

    $('#btnSubmiteuentrego').click(function() {

        $('#table-listagem-objetos-euentrego').DataTable().draw();
    });

    $('#btn-limpareuentrego').click(function() {
        $('#form-buscaeuentrego').each(function () {
            this.reset();
         });
     });






$(function () {


  $('.dataIniFim').daterangepicker({
       autoUpdateInput: false,
       locale: {

           "format": "DD/MM/YYYY",

           "separator": " - ",
           "applyLabel": "Aplicar",
           "cancelLabel": "Cancelar",
           "fromLabel": "From",
           "toLabel": "To",
           "customRangeLabel": "Custom",
           "daysOfWeek": [
               "Do",
               "Se",
               "Te",
               "Qu",
               "Qu",
               "Se",
               "Sa"
           ],
           "monthNames": [
               "Janeiro",
               "Fevereiro",
               "Março",
               "Abril",
               "Maio",
               "Junho",
               "Julho",
               "Agosto",
               "Setembro",
               "Outubro",
               "Novembro",
               "Dezembro"
           ],
       }
   });






   $('.dataIniFim').on('apply.daterangepicker', function(ev, picker) {
     $(this).val(picker.startDate.format('DD/MM/YYYY') + ' - ' + picker.endDate.format('DD/MM/YYYY'));
 });

 $('.dataIniFim').on('cancel.daterangepicker', function(ev, picker) {
     $(this).val('');
 });




  })
