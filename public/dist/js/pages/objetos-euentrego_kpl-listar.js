$(document).ready(function() {

  var table =  $('#table-listagem-pedidos').DataTable({
       "processing": true,
       "serverSide": true,
       "language": {
                "url": "https://cdn.datatables.net/plug-ins/1.10.12/i18n/Portuguese-Brasil.json"
            },
       "ajax": {
           "url": $('#table-listagem-pedidos').attr('action'),
           "type": "POST",
           "data" :  function ( d ) {
             return $.extend( {}, d,
               { '_token':$('input[name="_token"]').val() ,'dataIni' : $('#dataIni').val()
                            ,'dataFin' : $('#dataFin').val() , 'destinatario' : $('#destinatario').val(), 'codigo_kpl' : $('#codigo_kpl').val(), 'data_postagem' : $('#data_postagem').val()  }
             );
                }
        },

       "columns": [

           { "data": "id" },
           { "data": "numero" },
           { "data" : "codigo_rastreio"} ,
           { "data": "tray_id" },
           { "data": "data" },
           { "data": "valor" },
           { "data": "frete_pago" },
           { "data": "destinatario" }
       ]

   });

   $('#btnSubmit').click(function() {

       $('#table-listagem-pedidos').DataTable().draw();
   });

   $('#btn-limpar').click(function() {
       $('#form-busca').each(function () {
           this.reset();
        });
    });


  $('body').on( 'click' , '.btn-reenviopedido-tray' , function(){

        $idPedido = $(this).attr('codigoPed');
        $.ajax({
              type: 'POST' ,
              url: $('#form-reeenvio-pedido').val()+'/'+$idPedido ,
              data:  { '_token' : $('input[name="_token"]').val() } ,
          beforeSend: function( xhr ) {


           },
           success: function(retorno) {
             $('#error'+$idPedido).html('');
             $.bootstrapGrowl( retorno.msg ,{
                type: retorno.type ,
                delay: 4000,
             });
           }
        });
  })

});
