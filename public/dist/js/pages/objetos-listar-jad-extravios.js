
//console.log(document.getElementById('table-listagem-objetos_filter'));

//.children('input').placeholder = 'Novo tye';

$(document).ready(function() {



  var table =  $('#table-listagem-objetos').DataTable({
       "processing": true,
       "serverSide": true,
       "language": {
                "url": "https://cdn.datatables.net/plug-ins/1.10.12/i18n/Portuguese-Brasil.json",

            },

       "ajax": {
           "url": $('#table-listagem-objetos').attr('action'),
           "type": "POST",
           "data" :  function ( d ) {
             return $.extend( {}, d,
               { '_token':$('input[name="_token"]').val()


                         ,'dataIni' : $('#dataIni').val() ,'dataFin' : $('#dataFin').val()
                         ,'buscaEmails' : 'false'
                          ,'statusBuscar' : $('#status-buscarcorreios option:selected').val()
                         ,'cep_origem' : $('#cep_origemcorreios').val()
                         ,'cep_destino' : $('#cep_destinocorreios').val()
                         ,'destinatario' : $('#destinatariocorreios').val()
                         ,'data_postagem' : $('#data_postagemcorreios').val()
                         ,'data_considerada' :$('#data_consideradacorreios').val()
                         ,'data_prevista' :$('#data_previstacorreios').val()
                         ,'data_entrega' : $('#data_entregacorreios').val()
                         , 'transportadora' :  $('#transportadora option:selected').val()
                       }
                         );
                    }
                  }
           ,
       "columns": [

           { "data": "codigo_rastreio" },
           { "data": "data_postagem" },
           { "data": "destinatario" },
           { "data": "status" },
           { "data": "data" },
           { "data": "descricao" },
           { "data": "data_ultimo" },
           { "data": "posicao" }

       ]

   });

   $('#btnSubmitcorreios').click(function() {

       $('#table-listagem-objetos').DataTable().draw();
   });

   $('#btn-limparcorreios').click(function() {
       $('#form-buscacorreios').each(function () {
           this.reset();
        });
    });





// #myInput is a <input type="text"> element


   // Add event listener for opening and closing details
      $('#table-listagem-objetos tbody').on('click', 'td.details-control', function () {
          var tr = $(this).closest('tr');
          var row = table.row( tr );

          if ( row.child.isShown() ) {
              // This row is already open - close it
              row.child.hide();
              tr.removeClass('shown');
          }
          else {
              // Open this row
              row.child( format(row.data()) ).show();
              tr.addClass('shown');
          }
      });





      $('#btn-busca-periodo').on('click' ,function(event){


          event.preventDefault();


           $('#form-busca-periodo').submit() ;
           $('input[name="pg"]').val( parseInt($('input[name="pg"]').val( )) + 1 );

      });

      $('.buscaData').on('change' ,function(event){
        $.ajax({
              type:"POST",
             url:   $('#form-busca-periodo').attr('action') ,
             dataType:'json',
             data:{ pg : '-1' ,'_token':$('input[name="_token"]').val() ,dataIni : $('input[name="dataIni"]').val() ,dataFim : $('input[name="dataFim"]').val() },
             success: function(data){
                $('#totalPaginas').text(data.totalPaginas);
                $('#totalLinhas').text(data.t);
            }
        });
      });



/******************************************Busca jad**********/

var tableJad =  $('#table-listagem-objetos-jad').DataTable({
     "processing": true,
     "serverSide": true,
     "language": {
              "url": "https://cdn.datatables.net/plug-ins/1.10.12/i18n/Portuguese-Brasil.json"
          },
     "ajax": {
         "url": $('#table-listagem-objetos-jad').attr('action'),
         "type": "POST",
         "data" :  function ( d ) {
           return $.extend( {}, d,
             { '_token':$('input[name="_token"]').val()
                       ,'estado' :$('#buscar-por-estado').val()
                       ,'criticos' : $('#pagCriticos').val()
                       ,'mod' : $('#mod').val()
                       ,'dataIni' : $('#dataIni').val() ,'dataFin' : $('#dataFin').val()
                       ,'buscaEmails' : 'false'
                        ,'statusBuscar' : $('#status-buscarjad option:selected').val()
                       ,'cep_origem' : $('#cep_origemjad').val()
                       ,'cep_destino' : $('#cep_destinojad').val()
                       ,'destinatario' : $('#destinatariojad').val()
                       ,'data_postagem' : $('#data_postagemjad').val()
                       ,'data_considerada' :$('#data_consideradajad').val()
                       ,'data_prevista' :$('#data_previstajad').val()
                       ,'data_entrega' :$('#data_entregajad').val()
                       ,'protocolo' : $('#protocolo').val()
                       ,'comResposta' : $('#com-resposta').iCheck('update')[0].checked
                       ,'clientes_id' :  $('select[name="cliente"]').val()
                     }
                       );
                  }
     },
     "columns": [
          {
              "className":      'details-control',
              "orderable":      false,
              "data":           null,
              "defaultContent": ''
          },
         { "data": "id" },
         { "data": "codrastreio" },
         { "data": "qtdDiasPrevEntregaUteis" },
         { "data": "dataconsiderada" },
         { "data": "dataprevista" },
         { "data": "dataentrega" },
         { "data": "ceporigem" },
         { "data": "cepdestino" },
         { "data": "statusenvio" },
         { "data": "codigoreclamacao" },
        //   { "data": "enviou_xml" },
         {"data" : 'btnBetalhe' ,"orderable":      false,}
     ]

 });

 // Add event listener for opening and closing details
    $('#table-listagem-objetos-jad tbody').on('click', 'td.details-control', function () {
        var tr = $(this).closest('tr');
        var row = tableJad.row( tr );

        if ( row.child.isShown() ) {
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
        }
        else {
            // Open this row
            row.child( format(row.data()) ).show();
            tr.addClass('shown');
        }
    });

    $('#btnSubmitjad').click(function() {

        $('#table-listagem-objetos-jad').DataTable().draw();
    });

    $('#btn-limparjad').click(function() {
        $('#form-buscajad').each(function () {
            this.reset();
         });
     });

});

/**********************************************************************************/

/******************************************Busca euentrego****************************************************************************/

var tableJad =  $('#table-listagem-objetos-euentrego').DataTable({
     "processing": true,
     "serverSide": true,
     "language": {
              "url": "https://cdn.datatables.net/plug-ins/1.10.12/i18n/Portuguese-Brasil.json"
          },
     "ajax": {
         "url": $('#table-listagem-objetos-euentrego').attr('action'),
         "type": "POST",
         "data" :  function ( d ) {
           return $.extend( {}, d,
             { '_token':$('input[name="_token"]').val()
                       ,'estado' :$('#buscar-por-estado').val()
                       ,'criticos' : $('#pagCriticos').val()
                       ,'mod' : $('#mod').val()
                       ,'dataIni' : $('#dataIni').val() ,'dataFin' : $('#dataFin').val()
                       ,'buscaEmails' : 'false'
                        ,'statusBuscar' : $('#status-buscareuentrego option:selected').val()
                       ,'cep_origem' : $('#cep_origemeuentrego').val()
                       ,'cep_destino' : $('#cep_destinoeuentrego').val()
                       ,'destinatario' : $('#destinatarioeuentrego').val()
                       ,'data_postagem' : $('#data_postagemeuentrego').val()
                       ,'data_considerada' :$('#data_consideradaeuentrego').val()
                       ,'data_prevista' :$('#data_previstaeuentrego').val()
                       ,'data_entrega' :$('#data_entregaeuentrego').val()
                       ,'protocolo' : $('#protocolo').val()
                       ,'comResposta' : $('#com-resposta').iCheck('update')[0].checked
                       ,'clientes_id' :  $('select[name="cliente"]').val()
                     }
                       );
                  }
     },
     "columns": [
          {
              "className":      'details-control',
              "orderable":      false,
              "data":           null,
              "defaultContent": ''
          },
         { "data": "id" },
         { "data": "codrastreio" },
         { "data": "qtdDiasPrevEntregaUteis" },
         { "data": "dataconsiderada" },
         { "data": "dataprevista" },
         { "data": "dataentrega" },
         { "data": "ceporigem" },
         { "data": "cepdestino" },
         { "data": "statusenvio" },
         { "data": "codigoreclamacao" },
        //   { "data": "enviou_xml" },
         {"data" : 'btnBetalhe' ,"orderable":      false }
     ]

 });

 // Add event listener for opening and closing details
    $('#table-listagem-objetos-euentrego tbody').on('click', 'td.details-control', function () {
        var tr = $(this).closest('tr');
        var row = tableJad.row( tr );

        if ( row.child.isShown() ) {
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
        }
        else {
            // Open this row
            row.child( format(row.data()) ).show();
            tr.addClass('shown');
        }
    });

    $('#btnSubmiteuentrego').click(function() {

        $('#table-listagem-objetos-euentrego').DataTable().draw();
    });

    $('#btn-limpareuentrego').click(function() {
        $('#form-buscaeuentrego').each(function () {
            this.reset();
         });
     });






$(function () {


  $('.dataIniFim').daterangepicker({
       autoUpdateInput: false,
       locale: {

           "format": "DD/MM/YYYY",

           "separator": " - ",
           "applyLabel": "Aplicar",
           "cancelLabel": "Cancelar",
           "fromLabel": "From",
           "toLabel": "To",
           "customRangeLabel": "Custom",
           "daysOfWeek": [
               "Do",
               "Se",
               "Te",
               "Qu",
               "Qu",
               "Se",
               "Sa"
           ],
           "monthNames": [
               "Janeiro",
               "Fevereiro",
               "Março",
               "Abril",
               "Maio",
               "Junho",
               "Julho",
               "Agosto",
               "Setembro",
               "Outubro",
               "Novembro",
               "Dezembro"
           ],
       }
   });






   $('.dataIniFim').on('apply.daterangepicker', function(ev, picker) {
     $(this).val(picker.startDate.format('DD/MM/YYYY') + ' - ' + picker.endDate.format('DD/MM/YYYY'));
 });

 $('.dataIniFim').on('cancel.daterangepicker', function(ev, picker) {
     $(this).val('');
 });




  })
