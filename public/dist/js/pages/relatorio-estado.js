$(document).ready(function(){
      $.ajax({
            url: $('#rotaDadosGraficosCidades').val(),
            data: {dataIni : $('#busca-dataIni-dia').val() , dataFim: $('#busca-dataFim-dia').val(),retorno_json:true ,_token : $('input[name="_token"]').val() },

        beforeSend: function( xhr ) {

            $('#box-grafico').prepend('<div class="overlay"><i class="fa fa-refresh fa-spin"></i></div>');
         },
      success: function(retorno) {

        var data=[];
        var colors=[];
        var Header=  ['Cidade' ,  'Total'];
        data.push(Header);
        for (var i = 0; i < retorno.length; i++) {

                var temp=[];
                temp.push(retorno[i].cidade);
                temp.push(parseInt(retorno[i].totalCidade) > 0 ? parseInt(retorno[i].totalCidade) : 0 );

                data.push(temp);

            }

          google.charts.load('current', {'packages':['corechart']});
          google.charts.setOnLoadCallback(drawVisualization(data));

          function drawVisualization(data) {
            // Some raw data (not necessarily accurate)

            console.log(data);
            var data = google.visualization.arrayToDataTable(data);

          var options = {
          title : 'Cidades de destino',
          seriesType: 'bars',
          series: {4: {type: 'line'}}
          };

          var chart = new google.visualization.ComboChart(document.getElementById('grafico-qtd-cidades'));
          chart.draw(data, options);
          }

          $('#box-grafico .overlay').remove();

      }
    });



})
