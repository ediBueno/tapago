
function format ( d ) {
    // `d` is the original data object for the row
  if($('#pagCriticos').val()){ //se for a listagem de objetos criticos

    return '<table cellpadding="10" class="table" cellspacing="0" border="0" >'+
                '<tr>'+
                    '<th>Dt. Cons:</th>'+
                    '<th>Cep Orig</th>'+
                    '<th>Cep Dest</th>'+
                    '<th>Alerta</th>'+
                    '<th>Reclamação</th>'+
                '</tr>'+
                '<tr>'+
                    '<td>'+d.dataconsiderada+'</td>'+
                    '<td>'+d.ceporigem+'</td>'+
                      '<td>'+d.cepdestino+'</td>'+
                    '<td>'+(d.alerta != null ? d.alerta : '-' )+'</td>'+
                    '<td>'+(d.abrirreclamacao != null ? d.abrirreclamacao : '-' )+'</td>'+
                '</tr>'+
            '</table>';
          }else{

    return '<table cellpadding="10" class="table" cellspacing="0" border="0" >'+
                '<tr>'+
                    '<th>Ent domic&iacute;lio:</th>'+
                    '<th>Posição</th>'+
                    '<th>Alerta</th>'+
                    '<th>Reclamação</th>'+
                '</tr>'+
                '<tr>'+
                    '<td>'+d.entregadomiciliar+'</td>'+
                    '<td>'+d.posicaodoobjeto+'</td>'+
                    '<td>'+(d.alerta != null ? d.alerta : '-' )+'</td>'+
                    '<td>'+(d.abrirreclamacao != null ? d.abrirreclamacao : '-' )+'</td>'+
                '</tr>'+
            '</table>';

          }
}

function modalMovimentacoesRastreio(url , numero) {

  $.ajax({
      url: url ,
     dataType:'json',
      data:{ codRastreio: numero ,'_token':$('input[name="_token"]').val() },
      success: function(data){

      $lista =   '<ul class="timeline">';



        $.each(data['dados'], function (key, dados) {

          if((dados.tipo == 'BDE' || dados.tipo == 'BDI' || dados.tipo == 'BDR') && (dados.statuss == 0 || dados.statuss == 1 )) {
            icone = 'fa-home' ;
          }else {
            icone =  'fa-check-square-o'   ;
          }
          $lista +=     '<li class="time-label">'+
                            '<span class="bg-red">'+
                              dados.datas+
                            '</span>'+
                      '</li>'+
                      '<li>'+
                        '<i class="fa ' + icone +' bg-blue"></i>'+
                        '<div class="timeline-item">'+
                          '<span class="time"><i class="fa fa-clock-o"></i>'+dados.hora+'</span>'+
                          '<h3 class="timeline-header"><a href="#">'+dados.descricao+'</a> TIPO : '+dados.tipo+'      STATUS : '+dados.statuss+'</h3>'+
                          '</div>'+
                      '</li>';
              });

              $lista +=    '</ul>';


          $('#modal-default .modal-header h4').text('Cod Registro: '+numero);
          $('#modal-default .modal-body').html($lista);
          $('#modal-default .modal-footer').remove();
          $('#modal-default').modal();
      }
  });

}

$(document).ready(function() {
if($('#pagCriticos').val()){

$('#table-listagem-objetos thead').html('<tr>'+
                                              '<th></th>'+
                                              '<th>#</th>'+
                                              '<th>Cod Reg</th>'+
                                              '<th>Dias</th>'+
                                              '<th>Dt Prevista</th>'+
                                              '<th>Dt Entrega</th>'+
                                              '<th>Status</th>'+
                                              '<th>Posição</th>'+
                                              '<th>Ent domic&iacute;lio</th>'+
                                              '<th>Prot</th>'+
                                              '<th>Ep</th>'+
                                              '<th></th>'+
                                          '</tr>');

  var table =  $('#table-listagem-objetos').DataTable({
       "processing": true,
       "serverSide": true,
       "language": {
                "url": "https://cdn.datatables.net/plug-ins/1.10.12/i18n/Portuguese-Brasil.json"
            },
       "ajax": {
           "url": $('#table-listagem-objetos').attr('action'),
           "type": "POST",
           "data" :{ '_token':$('input[name="_token"]').val() ,
                      'estado' :$('#buscar-por-estado').val() ,'criticos' : $('#pagCriticos').val() ,'statusBuscar' :$('#status').val()  }
       },
       "columns": [
            {
                "className":      'details-control',
                "orderable":      false,
                "data":           null,
                "defaultContent": ''
            },
           { "data": "idenvio" },
           { "data": "codrastreio" },
           { "data": "qtdDiasPrevEntregaUteis" },
           { "data": "dataprevista" },
           { "data": "dataentrega" },
           { "data": "statusenvio" },
           { "data": "posicaodoobjeto" },
            { "data": "entregadomiciliar" },
           { "data": "codigoreclamacao" },
            { "data": "enviou_xml" },
           {"data" : 'btnBetalhe' ,"orderable":      false,}
       ]

   });
}else{
  var table =  $('#table-listagem-objetos').DataTable({
       "processing": true,
       "serverSide": true,
       "language": {
                "url": "https://cdn.datatables.net/plug-ins/1.10.12/i18n/Portuguese-Brasil.json"
            },
       "ajax": {
           "url": $('#table-listagem-objetos').attr('action'),
           "type": "POST",
           "data" :{ '_token':$('input[name="_token"]').val() ,
                      'estado' :$('#buscar-por-estado').val() ,'criticos' : $('#pagCriticos').val() ,'statusBuscar' :$('#status').val()  }
       },
       "columns": [
            {
                "className":      'details-control',
                "orderable":      false,
                "data":           null,
                "defaultContent": ''
            },
           { "data": "idenvio" },
           { "data": "codrastreio" },
           { "data": "qtdDiasPrevEntregaUteis" },
           { "data": "dataconsiderada" },
           { "data": "dataprevista" },
           { "data": "dataentrega" },
           { "data": "ceporigem" },
           { "data": "cepdestino" },
           { "data": "statusenvio" },
           { "data": "codigoreclamacao" },
             { "data": "enviou_xml" },
           {"data" : 'btnBetalhe' ,"orderable":      false,}
       ]

   });

}
   // Add event listener for opening and closing details
      $('#table-listagem-objetos tbody').on('click', 'td.details-control', function () {
          var tr = $(this).closest('tr');
          var row = table.row( tr );

          if ( row.child.isShown() ) {
              // This row is already open - close it
              row.child.hide();
              tr.removeClass('shown');
          }
          else {
              // Open this row
              row.child( format(row.data()) ).show();
              tr.addClass('shown');
          }
      } );
});
