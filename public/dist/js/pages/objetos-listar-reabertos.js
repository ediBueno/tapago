
function format ( d ) {
    // `d` is the original data object for the row

    return '<table cellpadding="10" class="table" cellspacing="0" border="0" >'+
                '<tr>'+
                    '<th>Ent domic&iacute;lio:</th>'+
                    '<th>Posição</th>'+
                    '<th>Alerta</th>'+
                    '<th>Reclamação</th>'+
                '</tr>'+
                '<tr>'+
                    '<td>'+d.entregadomiciliar+'</td>'+
                    '<td>'+d.posicaodoobjeto+'</td>'+
                    '<td>'+(d.alerta != null ? d.alerta : '-' )+'</td>'+
                    '<td>'+(d.abrirreclamacao != null ? d.abrirreclamacao : '-' )+'</td>'+
                '</tr>'+
            '</table>';


}

function modalMovimentacoesRastreio(url , numero) {

  $.ajax({
      url: url ,
     dataType:'json',
      data:{ codRastreio: numero ,'_token':$('input[name="_token"]').val() },
      success: function(data){

      $lista =   '<ul class="timeline">';



        $.each(data['dados'], function (key, dados) {

          if((dados.tipo == 'BDE' || dados.tipo == 'BDI' || dados.tipo == 'BDR') && (dados.statuss == 0 || dados.statuss == 1 )) {
            icone = 'fa-home' ;
          }else {
            icone =  'fa-check-square-o'   ;
          }
          $lista +=     '<li class="time-label">'+
                            '<span class="bg-red">'+
                              dados.datas+
                            '</span>'+
                      '</li>'+
                      '<li>'+
                        '<i class="fa ' + icone +' bg-blue"></i>'+
                        '<div class="timeline-item">'+
                          '<span class="time"><i class="fa fa-clock-o"></i>'+dados.hora+'</span>'+
                          '<h3 class="timeline-header"><a href="#">'+dados.descricao+'</a> TIPO : '+dados.tipo+'      STATUS : '+dados.statuss+'</h3>'+
                          '</div>'+
                      '</li>';
              });

              $lista +=    '</ul>';


          $('#modal-default .modal-header h4').text('Cod Registro: '+numero);
          $('#modal-default .modal-body').html($lista);
          $('#modal-default .modal-footer').remove();
          $('#modal-default').modal();
      }
  });

}

$(document).ready(function() {

  var table =  $('#table-listagem-objetos').DataTable({
       "processing": true,
       "serverSide": true,
       "language": {
                "url": "https://cdn.datatables.net/plug-ins/1.10.12/i18n/Portuguese-Brasil.json"
            },
       "ajax": {
           "url": $('#table-listagem-objetos').attr('action'),
           "type": "POST",
           "data" :{ '_token':$('input[name="_token"]').val() ,
                      'estado' :$('#buscar-por-estado').val() ,'reabertos' : 'true' ,'statusBuscar' :$('#status').val()  }
       },
       "columns": [
            {
                "className":      'details-control',
                "orderable":      false,
                "data":           null,
                "defaultContent": ''
            },
           { "data": "idenvio" },
           { "data": "codrastreio" },
           { "data": "qtdDiasPrevEntregaUteis" },
           { "data": "dataconsiderada" },
           { "data": "dataprevista" },
           { "data": "dataentrega" },
           { "data": "ceporigem" },
           { "data": "cepdestino" },
           { "data": "statusenvio" },
           { "data": "codigoreclamacao" },
             { "data": "enviou_xml" },
           {"data" : 'btnBetalhe' ,"orderable":      false,}
       ]

   });


   // Add event listener for opening and closing details
      $('#table-listagem-objetos tbody').on('click', 'td.details-control', function () {
          var tr = $(this).closest('tr');
          var row = table.row( tr );

          if ( row.child.isShown() ) {
              // This row is already open - close it
              row.child.hide();
              tr.removeClass('shown');
          }
          else {
              // Open this row
              row.child( format(row.data()) ).show();
              tr.addClass('shown');
          }
      });


      $('#btn-busca-periodo').on('click' ,function(event){


          event.preventDefault();
          $('input[name="pg"]').val( parseInt($('input[name="pg"]').val( )) + 1 );
           $('#form-busca-periodo').submit() ;

      });

      $('.buscaData').on('change' ,function(event){
        $.ajax({
              type:"POST",
             url:   $('#form-busca-periodo').attr('action') ,
             dataType:'json',
             data:{ pg : '-1' ,'_token':$('input[name="_token"]').val() ,dataIni : $('input[name="dataIni"]').val() ,dataFim : $('input[name="dataFim"]').val() },
             success: function(data){
                $('#totalPaginas').text(data.totalPaginas);
                $('#totalLinhas').text(data.t);
            }
        });
      });

});
