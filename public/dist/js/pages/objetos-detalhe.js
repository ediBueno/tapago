$(document).ready(function() {

  $('#btnSubmitEmail').click(function() {

          if($('#assuntoEmail').val() == ''){
             $('#div-assunto').addClass('has-error');
             $('#div-assunto .help-block').remove();
             $('#div-assunto').append('<span class="help-block">Digite o assunto!</span>');
             return false;
          }

          if($('#descricaoEmail').val() == ''){
             $('#div-descricao-email').addClass('has-error');
             $('#div-descricao-email .help-block').remove();
             $('#div-descricao-email').append('<span class="help-block">Digite a descrição do email!</span>');
             return false;
          }

         $codigoRemove = $(this).attr('codigo');
        $.ajax({
            url: $('#route-cademail').val()+"/"+$(this).attr('codigo')+"" ,
           dataType:'json',
           type: "POST",
            data:{ '_token': $('input[name="_token"]').val() ,
                  assunto :$('#assuntoEmail').val()  ,
                  descricao : $('#descricaoEmail').val()  ,
                  data :  $('#dataEmail').val()  ,
                  tipo :  $('#tipoEmail').val()
            },
            success: function(retorno){

                $.bootstrapGrowl( retorno.msg ,{
                   type: retorno.type ,
                   delay: 4000,
                });


                $('#timeline-email').html(retorno.dados.emails.timeline);
                $('#btn-limparEmail').click();
            }
          })


  });

  $('#btn-limparEmail').click(function() {
      $('#form-cadEmails').each(function () {
          this.reset();
       });
   });

  $('#my-box').boxWidget('toggle');

    $('#btn-busca-periodo').click(function(){
        if($('#busca-objeto').val() != ''){
          $(location).attr('href', $(this).attr('action')+'/'+$('#busca-objeto').val());
        }else {
          $('#busca-objeto').focus()
        }
     })

    $.ajax({
        url: $('#url-dados-objeto').val() ,
       dataType:'json',
       type: "POST",
        data:{ '_token':$('input[name="_token"]').val() ,start:'0' ,length:'1' ,objeto:$('#num-objeto').val()},
        success: function(data){
            $('#info-posicao-objeto').html('<i class="icon fa fa-check"></i> '+data['data'][0].posicaodoobjeto);

            if (data['data'][0].statusenvio == 'DEVOLVIDO') {
              $('#btnInfoDevolvido').removeClass('hide');
            }else {

              $('#btnInfoDevolvido').remove();
              if (data['data'][0].statusenvio == 'NAO RECEBEU') {
                $('#info-status-envio').removeClass('hide');
                $('#info-status-envio').html('<i class="icon fa fa-close"></i> '+data['data'][0].statusenvio);
              }


            }

            $('#info-tipo-postagem').html('<i class="icon fa fa-ticket"></i> '+data['data'][0].servicoDeEnvio+' '+data['data'][0].custopostagem);

            if(data['data'][0].codigoreclamacao > 0){

              $('#info-reclamacao').html('<i class="icon fa fa-close"></i> '+data['data'][0].codigoreclamacao+' data abertura: '+data['data'][0].datareclamacao);

            }

            $('#data-postagem').text(data['data'][0].datapostagem);
            $('#data-considerada').text(data['data'][0].dataconsiderada);

            $('#cep-postagem').text(data['data'][0].ceporigem);
            $('#cidade-postagem').text(data['data'][0].cidadeOrigem+' - '+data['data'][0].ufOrigem);

            $('#cep-destino').text(data['data'][0].cepdestino);
            $('#cidade-destino').text(data['data'][0].cidadeDestino+' - '+data['data'][0].ufDestino);

            $('#nome-destino').text(data['data'][0].destinatario);
            $('#rua-destino').text(data['data'][0].rua_destinatario);
            $('#bairro-destino').text(data['data'][0].bairro_destinatario);

            $('#previsao-data').text(data['data'][0].dataprevista);
            $('#previsao-dias').text(data['data'][0].qtdDiasPrevEntrega +' | '+ (data['data'][0].qtdDiasPrevEntregaUteis > 1 ? data['data'][0].qtdDiasPrevEntregaUteis+' dias úteis' : data['data'][0].qtdDiasPrevEntregaUteis+' dia útil' ));

            $('#entregue-em').text(data['data'][0].dataentrega);
            $('#total-de-dias').text(data['data'][0].totalDeDias+' | '+ (data['data'][0].totalDeDiasUteis > 1 ? data['data'][0].totalDeDiasUteis+ ' dias úteis' : data['data'][0].totalDeDiasUteis+ ' dia útil')  );

            movimentacoes = '';
            console.log(data['data'][0].movimentacoes);
            $.each( data['data'][0].movimentacoes , function(key , value){
                movimentacoes += '<tr><td>'+value.descricao+'</td><td>'+value.datas+'</td><td>'+value.cidade_origem+' - '+value.uf_origem+' - '+value.local_origem+'</td></tr>';
            })

          $('#table-movimentacoes tbody').html(movimentacoes);


        }
    });

    $('#btnEnvDevolvido').click(function(){

        $.ajax({
            url: $('#url-nao-recebeu').val() ,
            dataType:'json',
            type: "POST",
            data:{ '_token':$('input[name="_token"]').val() ,'protocolo' : $('#num-objeto').val() , 'abrirReclamacao' : $('#checkAbrirReclamacao').val() },
            beforeSend : function(){
                $('#textInfo').text('Aguarde...');
            },
            success: function(data){

                if (data.success == true ) {
                    $('#modalInfoDevolvido').modal('hide');
                    $('#info-status-envio').html('<i class="icon fa fa-close"></i> '+data.status);
                    $('#info-status-envio').removeClass('hide');
                    $('#btnInfoDevolvido').remove();
                    $('#textInfo').text('');
                }else{
                    $('#textInfo').text('Erro ao tentar alterar,tente novamente mais tarde!');
                }
            }
        });
    });

});
