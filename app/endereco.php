<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class endereco extends Model
{
    const CREATED_AT = 'data_cadastro';
    const UPDATED_AT = 'data_alteracao';
    public $table = 'pessoa_endereco';
    public $fillable = ['endereco' ,'numero_endereco' ,'complemento' ,'bairro' ,'cidade' ,'uf' , 'cod_pessoa'];
}
