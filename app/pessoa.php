<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\endereco;

class pessoa extends Model
{

      public $table = 'pessoa';
      const CREATED_AT = 'data_cadastro';


      const UPDATED_AT = 'data_alteracao';
      public $fillable = ['nome' ,'cpf' ,'data_nascimento' ,'telefone' ,'email'];


      public function enderecos()
      {
         return $this->hasMany(endereco::class  ,'cod_pessoa' ,'id');

      }
}
