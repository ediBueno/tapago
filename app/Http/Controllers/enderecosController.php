<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\pessoa;
use App\endereco;

class enderecosController extends Controller
{

    public function show(endereco $endereco)
    {
      return $endereco;
    }


    public function create(pessoa $pessoa ,Request $request)
    {
        $dados = $request->only('uf' ,'cidade' ,'bairro' ,'endereco' ,'numero_endereco' ,'complemento');

        $dados['cod_pessoa'] = $pessoa->id;

        $end = endereco::create($dados);

        return ['msg' => 'Endereço criado com sucesso!' ,'endereco' => $end];


    }

    public function update(endereco $endereco ,Request $request)
    {
      $dados = $request->only('uf' ,'cidade' ,'bairro' ,'endereco' ,'numero_endereco' ,'complemento');
      $endereco->update($dados);

      return ['msg' => 'Endereço alterado com sucesso!' ,'endereco' => $endereco];
    }

    public function destroy(endereco $endereco)
    {
          $endereco->delete();

          return ['msg' => 'Endereço excluido com sucesso!'];
    }


}
