<?php

namespace App\Http\Controllers;

use App\pessoa;
use Illuminate\Http\Request;

class pessoaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

          return view('pessoa.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {

            $dados = $request->only('nome' ,'cpf' ,'data_nascimento' ,'telefone' ,'email');

            if ($dados['nome'] == '') {
              return ['msg' => 'O campo nome não pode ser vazio!' ,'pessoa' => $pessoa ];
            }

            $pessoa = pessoa::create($dados);

            return ['msg' => 'Pessoa cadastrada com sucesso!' ,'pessoa' => $pessoa ];

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


        $dadosArrays = [];

        if (isset($request->input('search')['value']) AND !empty($request->input('search')['value'])) {

            $pessoas = pessoa::whereRaw('( nome LIKE "%'.$request->input('search')['value'].'%" OR telefone LIKE "%'.$request->input('search')['value'].'%"  )');
        }else{
            $pessoas = pessoa::select('pessoa.*') ;
        }

        $pessoas->offset($request->input('start'))->take($request->input('length'))
                    ->orderBy('id' ,'DESC');

        foreach ($pessoas->get() as  $row) {
                  $dadosArrays[] = [
                      'id'  => $row->id ,
                      'nome' =>  $row->nome ,
                      'cpf' =>   $row->cpf ,
                      'data_nascimento' =>  $row->data_nascimento ,
                      'telefone' => $row->telefone ,
                      'email' => $row->email ,
                      'enderecos' =>  $row->enderecos ,
                      'btnEditar' => '<a type="button" class="btn btn-warning" href="'.Route('pessoa.show' ,$row->id).'"  name="button">Editar</a>'
                   ];
            }

      return response()->json([
                "draw" => $request->input('draw'),
                "recordsTotal" => $pessoas->count()  ,
                "recordsFiltered" => $pessoas->count()   ,
                "data" =>       $dadosArrays

              ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\pessoa  $pessoa
     * @return \Illuminate\Http\Response
     */
    public function show(pessoa $pessoa)
    {

          return view('pessoa.editar' ,compact('pessoa'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\pessoa  $pessoa
     * @return \Illuminate\Http\Response
     */
    public function edit(pessoa $pessoa)
    {

          $dadosArrays = [
              'id'  => $pessoa->id ,
              'nome' =>  $pessoa->nome ,
              'cpf' =>   $pessoa->cpf ,
              'data_nascimento' =>  $pessoa->data_nascimento ,
              'telefone' => $pessoa->telefone ,
              'email' => $pessoa->email ,
              'enderecos' =>  $pessoa->enderecos
           ];
        return $dadosArrays;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\pessoa  $pessoa
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, pessoa $pessoa)
    {
          $dados = $request->only('nome' ,'cpf' ,'data_nascimento' ,'telefone' ,'email');

          if ($dados['nome'] == '') {
            return ['msg' => 'O campo nome não pode ser vazio!' ,'pessoa' => $pessoa ];
          }
          $pessoa->update($dados);

          return ['msg' => 'Pessoa alterada com sucesso!' ,'pessoa' => $pessoa ];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\pessoa  $pessoa
     * @return \Illuminate\Http\Response
     */
    public function destroy(pessoa $pessoa)
    {
        $pessoa->delete();

    }
}
