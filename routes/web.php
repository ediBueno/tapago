<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('pessoa.index');
});

   Route::group(['prefix' => 'pessoa'] , function () {

        Route::get('/listar',  ['uses' => 'pessoaController@index' ])
                ->name('pessoa.index');

        Route::get('/busca',  ['uses' => 'pessoaController@store' ])
                ->name('pessoa.store');

        Route::get('/editar/{pessoa}',  ['uses' => 'pessoaController@show' ])
                ->name('pessoa.show');




   });
