<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['prefix' => 'pessoa'] , function () {
     Route::get('/listar', function () {

     });
     Route::post('/cadastrar',  ['uses' => 'pessoaController@create' ])
             ->name('pessoa.create');

     Route::get('/{pessoa}',  ['uses' => 'pessoaController@edit' ])
             ->name('pessoa.edit');
     Route::put('/{pessoa}',  ['uses' => 'pessoaController@update' ])
             ->name('pessoa.update');
     Route::delete('/{pessoa}',  ['uses' => 'pessoaController@destroy' ])
             ->name('pessoa.delete');
});


Route::group(['prefix' => 'endereco'] , function () {

     Route::post('/cadastrar/{pessoa?}',  ['uses' => 'enderecosController@create' ])
             ->name('endereco.create');

     Route::put('/{endereco?}',  ['uses' => 'enderecosController@update' ])
             ->name('endereco.update');

     Route::delete('/{endereco?}',  ['uses' => 'enderecosController@destroy' ])
             ->name('endereco.delete');
     Route::get('/{endereco?}',  ['uses' => 'enderecosController@show' ])
             ->name('endereco.show');
});
